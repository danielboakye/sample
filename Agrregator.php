<?php 

class Aggregator extends MySqlCRUD
{
	/**
	 * insert record into mysql db
	 *
	 * @param string $tableName: table name
	 * @param array $bindings: key and value pair bindings for query
	 * @param string $columns: column names
	 * @param string $columnValues: value bindings paired in $bindings array
	 * @param boolean $return: specify return type
	 *
	 * @return ( $return == true ) ? int : boolean 
	 */

	public function addRecord($tableName, $bindings, $columns, $columnValues, $return=false){
		$query = sprintf("INSERT INTO %s ( %s ) VALUES ( %s )", $tableName, $columns, $columnValues );

		if(!empty($return)){
			return $this->returnInsertData($query, $bindings);
		}
		else{
			return $this->insertData($query, $bindings);
		}
	}


	/**
	 * load records from mysql db
	 *
	 * @param string $columns: column names
	 * @param string $tableName: table name
	 * @param string $target: value bindings paied in $targetArr array
	 * @param array $targetArr: key and value pair bindings for query
	 *
	 * @param array $search: append search to query  
	 * :: usage ::
	 * array("AND ( ColumnName LIKE :s OR ColumnName LIKE :s OR ColumnName LIKE :s )", $s),
	 *
	 * @param string $order: append order to query >> eg: ORDER BY ColumnName ASC
	 * @param string $limit: append limit to query >> eg: LIMIT 5
	 *
	 * @return multidimensional array
	 */

	public function loadAllRecords($columns, $tableName, $target="", $targetArr=array(), $search=array(), $order="", $limit=""){

		$bindings=[];
		if(!empty($target)){
			foreach ($targetArr as $key => $value) {
				$bindings[$key] = htmlspecialchars($value);
			}
		}

		if(!empty($search)){
			$searchStr=$search[0];
			$bindings["skey"]= '%' . $search[1] . '%';
		}else {
			$searchStr="";
		}

		$query = sprintf("SELECT %s FROM %s WHERE %s %s %s %s", $columns, $tableName, $target, $searchStr, $order, $limit );
		// echo $query;exit;
		return $this->getBigData($query, $bindings);
	}


	/**
	 * load one record from mysql db
	 *
	 * @param string $columns: column names
	 * @param string $tableName: table name
	 * @param string $target: value bindings paied in $targetArr array
	 * @param array $targetArr: key and value pair bindings for query
	 *
	 * @return array
	 */

	public function loadOneRecord($columns, $tableName, $target="", $targetArr=array()){

		$bindings=[];
		if(!empty($target)){
			foreach ($targetArr as $key => $value) {
				$bindings[$key] = htmlspecialchars($value);
			}
		}

		$query = sprintf("SELECT %s FROM  %s WHERE %s LIMIT 1", $columns, $tableName, $target );
		// echo $query;exit;
		return $this->getData($query, $bindings);

	}


	/**
	 * update user records in mysql db
	 *
	 * @param string $tableName: table name
	 * @param array $bindings: key and value pair bindings for query
	 * @param string $queryString: value bindings paied in $bindings array
	 * @param string $target: update target string added to userID Column
	 *
	 * @return boolean
	 */

	public function updateRecord($tableName, $bindings, $queryString, $target=""){
		$query = sprintf("UPDATE %s SET %s WHERE userID = :userID %s", $tableName, $queryString, $target);
		// echo $query;die();
		return $this->updateData($query, $bindings);
	}


	/**
	 * update records in mysql db
	 *
	 * @param string $tableName: table name
	 * @param array $bindings: key and value pair bindings for query
	 * @param string $queryString: value bindings paied in $bindings array
	 * @param string $target: update target string
	 *
	 * @return boolean
	 */

	public function genericUpdate($tableName, $bindings, $queryString, $target=""){
		$query = sprintf("UPDATE %s SET %s WHERE %s", $tableName, $queryString, $target);
		// echo $query;die();
		return $this->updateData($query, $bindings);
	}


	/**
	 * delete all user records in table in mysql db
	 *
	 * @param string $tableName: table name
	 *
	 * @return boolean
	 */

	public function resetRecord($tableName){
		$bindings=array('userID'=>$_SESSION['user']['userID'], 'server'=>$_SESSION['user']['server']);

		return $this->updateRecord($tableName, $bindings, "del = 1", "AND server = :server");
	}


	/**
	 * delete all records in table in mysql db
	 *
	 * @param string $tableName: table name
	 *
	 * @return boolean
	 */

	public function resetTable($tableName){
		return $this->genericUpdate($tableName, array(), "del = 1", "del = 0");
	}


	/**
	 * get records from lookup table
	 *
	 * @param string $key: categroy of records needed
	 *
	 * @return array
	 */

	public function lookup($key){
		return $this->loadAllRecords(
			$columns="EntityCode, EntityName", 
			$tableName="lookup", 
			$target="category = :category",
			$targetArr=array("category"=>$key),
			$search=[],
			$order="ORDER BY id ASC",
			$limit=""
		);
	}


	/**
	 * get all records from table
	 *
	 * @param string $key: categroy of records needed
	 * @param string $columns: column names
	 * @param string $id: order by column
	 *
	 * @return array
	 */

	public function loader($tableName, $columns="*", $id="id"){
		return $this->loadAllRecords(
			$columns, 
			$tableName, 
			$target="del = 0",
			$targetArr=array(),
			$search=[],
			$order="ORDER BY {$id} ASC",
			$limit=""
		);
	}


	/**
	 * get entity name of particular code from lookup on selected category
	 *
	 * @param string $category: categroy of records needed
	 * @param string $code: unique code on selected category
	 *
	 * @return string
	 */

	public function lookupName($category, $code){
		$data = $this->loadOneRecord(
			"EntityName", 
			"lookup", 
			"category = :category AND EntityCode = :EntityCode", 
			array('category'=>$category, 'EntityCode'=>$code)
		);

		if(!empty($data)){
			return strtoupper($data['EntityName']);
		}
		return $code;
	}



	/**
	 * get settings value on setting type
	 *
	 * @param string $type: setting type
	 * @param string $fallback: return fallback if setting is not found in db
	 *
	 * @return string
	 */

	public function settings($type, $fallback=""){
		$data = $this->loadOneRecord(
			"SettingData", 
			"log_settings", 
			"del = 0 AND SettingType = :SettingType", 
			array('SettingType'=>$type)
		);

		if(!empty($data)){
			return $data['SettingData'];
		}
		return $fallback;
	}




	/*  -----------------------------------------------------
	**  Scaping Content
	----------------------------------------------------- */


	public function ghanaWebContent($dom){
  		$html = $dom->find('#mainbody #medsection1', 0)->children(8)->innertext;
  		return htmlspecialchars($html);
	}

	public function joyOnlineContent($dom){
		$content="";
		foreach($dom->find('.container-990 .article-body .article-text p') as $ptag){
		    $content .= '<p>'.$ptag->innertext.'</p>';
		}
		return htmlspecialchars($content);
	}

	public function grahicOnlineContent($dom){
		
		$ConcateUrl = "https://www.graphic.com.gh";
		foreach($dom->find('.separated-grid .bd-postcontent-3 img') as $image){
			$image->src = $ConcateUrl . $image->src;
		}
		$dom->save();

		$content="";
		foreach($dom->find('.separated-grid .bd-postcontent-3 p') as $ptag){
			$content .= '<p>'.strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $ptag->innertext), '<b><a><img><strong>').'</p>';
		}
		return htmlspecialchars($content);
	}


	public function citiNewsContent($dom){

		foreach($dom->find('article.post .entry-content p a') as $links){
			$links->target = "_blank";
		}
		$dom->save();

		$content="";
		foreach($dom->find('article.post .entry-content p') as $ptag){
			$content .= '<p>'.strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $ptag->innertext), '<b><a><img><strong>').'</p>';
		}
		return htmlspecialchars($content);
	}


	public function peaceFMContent($dom){

		// $content="";
		// if( !empty($dom->find('table td img', 0)) ){
		// 	$content = "<div><center>" . $dom->find('table td img', 0) . "</center></div>";
		// }
		
		$content = "<p>" . $dom->find('span.peace_content_text_1', 0)->innertext . "</p>";

		return htmlspecialchars($content);
	}


	public function bAndFTContent($dom){

		$response = (object) array("PublicationDate"=>"", "html"=>"");

		foreach($dom->find('article.post .entry-content p a') as $links){
			$links->target = "_blank";
		}
		$dom->save();

		$content="";
		foreach($dom->find('article.post .td-post-content p') as $ptag){
			$content .= '<p>'.strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $ptag->innertext), '<b><a><img><strong>').'</p>';
		}
		
		$response->html = htmlspecialchars($content);
		$response->PublicationDate = $dom->find('article.post time.entry-date', 0)->datetime;

		return $response;
	}


	public function theChronicleContent($dom){
		
		foreach($dom->find('.col-md-8 .tt-content a') as $links){
			$links->target = "_blank";
		}
		$dom->save();

		$content = strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $dom->find('.col-md-8 .tt-content', 0)), '<b><a><p><strong><img>');
		return htmlspecialchars($content);
	}

	public function DCLeakerseContent($dom){

		$response = (object) array("Enclosure"=>"", "html"=>"");

		foreach($dom->find('.post-inner .entry a') as $links){
			$links->target = "_blank";
		}
		$dom->save();


		$response->Enclosure = !empty( $dom->find('.post-inner .entry p img', 0) ) ? strtok($dom->find('.post-inner .entry p img', 0)->src, '?') : "";

		// remove sharing
		if(!empty($dom->find('.post-inner .entry .sharedaddy', 0))){
			$dom->find('.post-inner .entry .sharedaddy', 0)->outertext = '';
		}
		
		if(!empty($dom->find('.post-inner .entry .jp-relatedposts', 0))){
			$dom->find('.post-inner .entry .jp-relatedposts', 0)->outertext = '';
		}


		$content = strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $dom->find('.post-inner .entry', 0)), '<b><a><p><strong><audio><img><iframe>');

		$response->html = htmlspecialchars($content);

		return $response;
	}


	public function GhCelebritiesContent($dom){

		foreach($dom->find('article.single a strong, article.single p strong') as $item) {
	    	$item->outertext = '';
	    }
	    
	    // remove caption
		if(!empty($dom->find('article.single .s-post-content figure.wp-caption', 0))){
			$dom->find('article.single .s-post-content figure.wp-caption', 0)->outertext = '';
		}

		foreach($dom->find('article.single .s-post-content a') as $links){
			$links->target = "_blank";
		}
		$dom->save();

		$content = strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $dom->find('article.single .s-post-content', 0)), '<b><a><p><strong><img><blockquote><iframe>');
		// echo str_replace("Source:", "", $content);exit;

		$content = str_replace('http://www.youtube.com', 'https://www.youtube.com', $content);

		return htmlspecialchars(str_replace(["Source:", "READ ALSO:"], "", $content));
	}


	public function HitxghContent($dom){

		$response = (object) array("PublicationDate"=>"", "html"=>"");

		foreach($dom->find('article.post .entry-content p a') as $links){
			$links->target = "_blank";
		}

		if( !empty($dom->find('article.post .entry-content p img', 0)) ){
			$dom->find('article.post .entry-content p img', 0)->outertext = '';
		}

		if( !empty($dom->find('article.post .entry-content center', 0)) ){
			$dom->find('article.post .entry-content center', 0)->outertext = '';
		}

		$dom->save();


		$children = count($dom->find('article.post .entry-content p'));

		$content="";
		foreach($dom->find('article.post .entry-content p') as $mk => $ptag){
			if( $mk >= ($children-2) ){
				break;
			}
			$content .= '<p>'.strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $ptag->innertext), '<b><a><img><strong><iframe>').'</p>';
		}

		$content = str_replace('http://www.youtube.com', 'https://www.youtube.com', $content);
		
		$response->html = htmlspecialchars($content);
		$response->PublicationDate = $dom->find('article.post time.entry-date', 0)->datetime;

		return $response;

	}

	public function ghanaMusicContent($dom){

		foreach($dom->find('.cs-post-wrapper .cs-post-content a') as $links){
			$links->target = "_blank";
		}
		$dom->save();

		if( !empty($dom->find('.cs-post-wrapper .cs-post-content img.jetpack-lazy-image', 0)) ){
			foreach($dom->find('.cs-post-wrapper .cs-post-content img.jetpack-lazy-image') as $img){
				$img->outertext = '';
			}
			
			foreach($dom->find('.cs-post-wrapper .cs-post-content .wp-caption-text') as $caption){
				$caption->outertext = '';
			}
		}

		$content="";
		foreach($dom->find('.cs-post-wrapper .cs-post-content p, .cs-post-wrapper .cs-post-content div.wp-caption') as $ptag){
			$content .= '<p>'.strip_tags( preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $ptag->innertext), '<b><a><img><strong><iframe>').'</p>';
		}

		return htmlspecialchars($content);
	}


	public function ghanaMotionContent($dom){

		foreach($dom->find('article.post .entry a') as $links){
			$links->target = "_blank";
		}

		if( !empty($dom->find('article.post .entry .share-post', 0)) ){
			$dom->find('article.post .entry .share-post', 0)->outertext = '';
		}
		$dom->save();

		if( !empty($dom->find('article.post .entry', 0)) ){
			$content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $dom->find('article.post .entry', 0));
		}

		return htmlspecialchars($content);
	}

	public function EveyoContent($dom){

		// echo $dom;exit;

		$response = (object) array("Enclosure"=>"", "html"=>"");

		foreach($dom->find('article.post .col-lg-10 .entry-content a') as $links){
			$links->target = "_blank";
		}
		$dom->save();


		if( !empty( $dom->find('article.post .herald-post-thumbnail span img', 0) ) ){
			$response->Enclosure = $dom->find('article.post .herald-post-thumbnail span img', 0)->src;

			if( empty(trim($dom->find('article.post .herald-post-thumbnail span img', 0)->srcset)) ){
				$response->Enclosure = "";
			}
		}


		$content = "";

		foreach($dom->find('.col-lg-10 .entry-content p, .col-lg-10 .entry-content h1, .col-lg-10 .entry-content h2, .col-lg-10 .entry-content h3, .col-lg-10 .entry-content h4') as $ptag){
			$content .= '<p>'. preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $ptag->innertext).'</p>';
		}

		$response->html = htmlspecialchars($content);

		return $response;

	}


	public function GoldStreetContent($dom){

		$enc_img = '';
		if( !empty( $dom->find('article.post .td-post-content .td-post-featured-image img', 0) ) ){
			$enc_img = $dom->find('article.post .td-post-content .td-post-featured-image img', 0)->src;
		}
		if( empty($enc_img) ){ return ''; }
		return $this->replace_content($enc_img);
	}


	/*  -----------------------------------------------------
	** end Scaping Content
	----------------------------------------------------- */


	/*  -----------------------------------------------------
	** Scaping Handler
	----------------------------------------------------- */

	public function getNewsContent($config){

		$CompressionGroup = array("3", "6");

		if( !in_array($config->PageFormat, $CompressionGroup) ){
			$dom = file_get_html($config->Url);
		}else{
			$ScrappedString = curl_get_contents($config->Url, $decompress=true);
			$dom = str_get_html($ScrappedString);
		}

		if ($dom !== false) {

			switch ($config->PageFormat) {
				case "1":
					// code...
					$content = $this->ghanaWebContent($dom);
					break;

				case "2":
					// code...
					$content = $this->joyOnlineContent($dom);
					break;

				case "3":
					// code...
					$content = $this->grahicOnlineContent($dom);
					break;

				case "4":
					// code...
					$content = $this->citiNewsContent($dom);
					break;

				case "5":
					$content = $this->peaceFMContent($dom);
					break;

				case "6":
					$content = $this->bAndFTContent($dom);
					break;

				case "7":
					$content = $this->theChronicleContent($dom);
					break;

				case "8":
					$content = $this->DCLeakerseContent($dom);
					break;

				case "9":
					$content = $this->GhCelebritiesContent($dom);
					break;

				case "10":
					$content = $this->HitxghContent($dom);
					break;
				case "11":
					$content = $this->ghanaMusicContent($dom);
					break;
				case "12":
					$content = $this->ghanaMotionContent($dom);
					break;
				case "13":
					$content = $this->EveyoContent($dom);
					break;
				case "14":
					$content = $this->GoldStreetContent($dom);
					break;
				
				default:
					// code...
					return $config->Fallback;
					break;
			}
		}

		return !empty($content) ? $content : $config->Fallback;
	}

	/*  -----------------------------------------------------
	** end Scaping Handler
	----------------------------------------------------- */



	/*  -----------------------------------------------------
	** Summary
	----------------------------------------------------- */

	public function graphicOnlineSummary($ScrapingUrl){

		// gzdecode -> hits errors sometimes --> encoding
		// $ScrappedString = gzdecode2(curl_get_contents($ScrapingUrl));

		$ScrappedString = curl_get_contents($ScrapingUrl, $decompress=true);

		$dom = str_get_html($ScrappedString);

		if ($dom !== false) {
			$html = array();

			$ConcateUrl = "https://www.graphic.com.gh";
			foreach ($dom->find('.separated-grid .separated-item-2') as $item) {

				$html[] = (object) array(
								"title" => trim( (string) $item->find('article.bd-article-9 h2.bd-postheader-23 a', 0)->innertext ),
								"pubDate" => (string) $item->find('article.bd-article-9 time', 0)->datetime,
								"description" => trim( (string) $item->find('article.bd-article-9 .bd-postcontent-7 h2', 0)->innertext),
								"enclosure" => $ConcateUrl . (string) $item->find('img', 0)->src,
								"link" => $ConcateUrl . (string) $item->find('article.bd-article-9 a', 0)->href,
								"guid" => $ConcateUrl . (string) $item->find('article.bd-article-9 a', 0)->href
							);
			}

			// dev_dump($html);exit;
			return $html;
		}

		return false;
	}

	public function citiNewsSummary($ScrapingUrl){

		$dom = file_get_html($ScrapingUrl);

		if ($dom !== false) {
			$html = array();

			foreach ($dom->find('.site-content ul li.post') as $item) {
				$html[] = (object) array(
								"title" => trim( (string) $item->find('h2.cat-grid-title a', 0)->innertext),
								"pubDate" => (string) $item->find('time.entry-date', 0)->datetime,
								"description" => trim( (string) $item->find('.entry-content p', 0)->innertext),
								"enclosure" => (string) $item->find('img', 0)->src,
								"link" => (string) $item->find('.post-thumbnail a', 0)->href,
								"guid" => (string) $item->find('.post-thumbnail a', 0)->href
							);
			}

			// dev_dump($html);exit;
			return $html;
		}

		return false;
	}


	public function peaceFMOnlineSummary($ScrapingUrl, $ConcateUrl){

		$dom = file_get_html($ScrapingUrl);

		if ($dom !== false) {
			$html = array();

			foreach ($dom->find('.news-listing table table.fin_dataTable tr') as $item) {

				if( !$item->find('a.peace_section_text_1', 0) ){
					continue;
				}

				$pubDate = trim( (string) $item->find('span.peace_section_text_1', 0)->innertext);
				$timestr = date("Y")."-".str_replace(":", "", $pubDate);
				$pubDate = date("Y-m-d H:i:s", strtotime($timestr));

				$html[] = (object) array(
								"title" => trim( (string) $item->find('a.peace_section_text_1', 0)->innertext),
								"pubDate" => $pubDate,
								"description" => '',
								"enclosure" => '',
								"link" => $ConcateUrl . (string) $item->find('a.peace_section_text_1', 0)->href,
								"guid" => $ConcateUrl . (string) $item->find('a.peace_section_text_1', 0)->href
							);
			}

			// dev_dump($html);exit;
			return $html;
		}

		return false;
	}


	private function replace_content($content) {
	    return preg_replace('/-([^-]*(\d+)(x|X)(\d+)\.((?:png|jpeg|jpg|gif|bmp)))/', '.${5}', $content);
	}

	private function getbnFTCleanImages($item){
		$enc_img = (string) $item->find('img.entry-thumb', 0)->src;

		if( empty($enc_img) ){ return ''; }
		return $this->replace_content($enc_img);
	}

	public function bnFTNewsSummary($ScrapingUrl){

		$ScrappedString = curl_get_contents($ScrapingUrl, $decompress=true);
		$dom = str_get_html($ScrappedString);
		// echo $dom;exit;

		if ($dom !== false) {
			$html = array();

			foreach ($dom->find('.td-theme-wrap .td-category-grid .td_block_inner .td-big-grid-post') as $item) {

				$html[] = (object) array(
								"title" => trim( (string) $item->find('.entry-title a', 0)->innertext ),
								"enclosure" => $this->getbnFTCleanImages($item),
								"link" => (string) $item->find('.entry-title a', 0)->href,
								"guid" => (string) $item->find('.entry-title a', 0)->href
							);
			}

			foreach ($dom->find('.td-theme-wrap .td-main-content-wrap .td-ss-main-content .td-block-span6') as $item) {

				$html[] = (object) array(
								"title" => trim( (string) $item->find('.entry-title a', 0)->innertext ),
								"enclosure" => $this->getbnFTCleanImages($item),
								"link" => (string) $item->find('.entry-title a', 0)->href,
								"guid" => (string) $item->find('.entry-title a', 0)->href
							);
			}

			return $html;
		}

		return false;
	}


	private function getChronicleImages($item){

		$enc_img = !empty($item->find('img')) ? (string) $item->find('img', 0)->src : '';

		if( empty($enc_img) ){ return ''; }
		return $this->replace_content($enc_img);

		// $enc_ext = trim(pathinfo($enc_img, PATHINFO_EXTENSION)); 
		// $file_path_array = explode("-", $enc_img);
		// if( count($file_path_array) > 1 ){
		// 	array_splice($file_path_array, -1);
		// }else{
		// 	return $enc_img;
		// }
		// return implode("-", $file_path_array) . ".{$enc_ext}";
	}

	public function theChronicleSummary($ScrapingUrl){

		$dom = file_get_html($ScrapingUrl);

		if ($dom !== false) {
			$html = array();

			foreach ($dom->find('.row .col-md-8 .tt-post') as $item) {

				$pubDate = trim( (string) $item->find('.tt-post-label span', 1)->innertext);
				$timestr = date("Y")."-".str_replace(" ", "-", $pubDate);	

				$html[] = (object) array(
								"title" => trim( (string) $item->find('a.tt-post-title', 0)->innertext),
								"pubDate" => $timestr,
								"description" => (string) $item->find('.simple-text p', 0)->innertext,
								"enclosure" =>  $this->getChronicleImages($item),
								"link" => (string) $item->find('a.tt-post-title', 0)->href,
								"guid" => (string) $item->find('a.tt-post-title', 0)->href
							);
			}

			return $html;
		}

		return false;
	}


	private function getGhCelebritiesCleanImages($item){
		$enc_img = (string) $item->find('a img', 0)->src;

		if( empty($enc_img) ){ return ''; }
		return $this->replace_content($enc_img);
	}

	public function ghanaCelebritiesSummary($ScrapingUrl){

		$dom = file_get_html($ScrapingUrl);

		if ($dom !== false) {
			$html = array();

			
			foreach ($dom->find('.featured-area-wrapper article.featured-item') as $item) {
				$html[] = (object) array(
								"title" => trim( (string) $item->find('h2.entry-title a', 0)->innertext),
								"pubDate" => (string) $item->find('time.entry-date', 0)->datetime,
								"enclosure" =>  $this->getGhCelebritiesCleanImages($item),
								"link" => (string) $item->find('a.featured-link', 0)->href,
								"guid" => (string) $item->find('a.featured-link', 0)->href
							);
			}

			foreach ($dom->find('ul.post-items article.post') as $item) {
				$html[] = (object) array(
								"title" => trim( (string) $item->find('h2.entry-title a', 0)->innertext),
								"pubDate" => (string) $item->find('time.entry-date', 0)->datetime,
								"enclosure" =>  $this->getGhCelebritiesCleanImages($item),
								"link" => (string) $item->find('h2.entry-title a', 0)->href,
								"guid" => (string) $item->find('h2.entry-title a', 0)->href
							);
			}

			return $html;
		}

		return false;
	}


	private function getHitxghCleanImages($item){
		$enc_img = (string) $item->find('a img', 0)->src;

		if( empty($enc_img) ){ return ''; }
		return 'https:' . $this->replace_content($enc_img);
	}

	public function HitxghSummary($ScrapingUrl){

		$dom = file_get_html($ScrapingUrl);

		if ($dom !== false) {
			$html = array();

			foreach ($dom->find('.post-item-grid-view article.post') as $item) {
				$html[] = (object) array(
								"title" => trim( (string) $item->find('h2.entry-title a', 0)->innertext),
								"enclosure" =>  $this->getHitxghCleanImages($item),
								"link" => (string) $item->find('h2.entry-title a', 0)->href,
								"guid" => (string) $item->find('h2.entry-title a', 0)->href
							);
			}

			return $html;
		}

		return false;

	}


	private function getGhMusicCleanImages($item){
		$enc_img = (string) $item->find('.cs-post-thumb a img', 0)->src;

		if( empty($enc_img) ){ return ''; }
		return preg_replace('/-([^-]*(\d+)(x|X)(\d+)_c\.((?:png|jpeg|jpg|gif|bmp)))/', '.${5}', $enc_img);
	}

	public function GhanaMusicSummary($ScrapingUrl){
		
		$dom = file_get_html($ScrapingUrl);

		if ($dom !== false) {
			$html = array();

			foreach ($dom->find('.cs-main-content article.post') as $item) {
				$html[] = (object) array(
								"title" => trim( (string) $item->find('.cs-post-wrapper h3 a', 0)->innertext),
								"pubDate" => (string) $item->find('.cs-post-meta-info time', 0)->datetime,
								"enclosure" =>  $this->getGhMusicCleanImages($item),
								"link" => (string) $item->find('.cs-post-wrapper h3 a', 0)->href,
								"guid" => (string) $item->find('.cs-post-wrapper h3 a', 0)->href
							);
			}

			return $html;
		}

		return false;
	}



	/*  -----------------------------------------------------
	** End Summary
	----------------------------------------------------- */



	/*  -----------------------------------------------------
	** Download Image Helper functions
	----------------------------------------------------- */

	private function getMiddlePath($NewsAgencyID){

		switch ($NewsAgencyID) {
			case "1":
				$middlepath = "uploads/ghanaweb/";
				break;
			case "2":
				$middlepath = "uploads/myjoyonline/";
				break;
			case "3":
				$middlepath = "uploads/graphiconline/";
				break;
			case "4":
				$middlepath = "uploads/citinewsroom/";
				break;
			case "5":
				$middlepath = "uploads/peacefm/";
				break;
			case "6":
				$middlepath = "uploads/bftonline/";
				break;
			case "7":
				$middlepath = "uploads/chronicle/";
				break;
			case "8":
				$middlepath = "uploads/dcleakers/";
				break;
			case "9":
				$middlepath = "uploads/ghcelebrities/";
				break;
			case "10":
				$middlepath = "uploads/hitxgh/";
				break;
			case "11":
				$middlepath = "uploads/ghanamusic/";
				break;
			case "12":
				$middlepath = "uploads/ghanamotion/";
				break;
			case "13":
				$middlepath = "uploads/eveyo/";
				break;
			case "14":
				$middlepath = "uploads/goldstreet/";
				break;
			default:
				// code...
				$middlepath="uploads/";
				break;
		}

		return $middlepath;
	}


	public function copyImage($itemSrc, $NewsAgencyID){

		$ext = trim(pathinfo($itemSrc, PATHINFO_EXTENSION)); 

		$supported = array("jpg", "jpeg", "gif", "png", "bmp");

		if( !in_array($ext, $supported) ){
			$ext = "jpg";
		}

		$filename = md5(uniqid()) . "." . strtolower($ext);

		$foldername = date("Y/m/d");
		
		$middlepath = $this->getMiddlePath($NewsAgencyID);
			

		$Document_Path = ROOT_PATH . $middlepath;
		$destination = $Document_Path . $foldername; 


		if (!file_exists($destination)) {
			mkdir($destination, 0755, true);

			$up_folders = explode("/", $foldername);
			if( !empty($up_folders) && count($up_folders) >= 2 ){
				file_put_contents($Document_Path . $up_folders[0] . "/index.html", "Not authorized <br>  Work Smart Limited");
				file_put_contents($Document_Path . $up_folders[0] . "/" . $up_folders[1] . "/index.html", "Not authorized <br>  Work Smart Limited");
			}
			file_put_contents($destination . "/index.html", "Not authorized <br>  Work Smart Limited");

		}

		$path = "{$destination}/{$filename}";

		if( file_exists($path) ){

			$senin = explode(".", $filename);
			$senin[0] .= "1";
			$filename = implode(".", $senin);
			$path = "{$destination}/{$filename}";
		}

		file_put_contents($path, curl_get_contents($itemSrc));

		return BASE_URL . "{$middlepath}{$foldername}/{$filename}";
	}


	public function generatePath($NewsAgencyID, $Resolution_Size=""){

		if(empty($Resolution_Size)){
			$filename = md5(uniqid()) . ".png";
		}
		else{
			$filename = md5(uniqid()) . ".{$Resolution_Size}.png";
		}

		$foldername = date("Y/m/d");
		
		$middlepath = $this->getMiddlePath($NewsAgencyID);
			
		$Document_Path = ROOT_PATH . $middlepath;
		$destination = $Document_Path . $foldername; 


		if (!file_exists($destination)) {
			mkdir($destination, 0755, true);

			$up_folders = explode("/", $foldername);
			if( !empty($up_folders) && count($up_folders) >= 2 ){
				file_put_contents($Document_Path . $up_folders[0] . "/index.html", "Not authorized <br>  Work Smart Limited");
				file_put_contents($Document_Path . $up_folders[0] . "/" . $up_folders[1] . "/index.html", "Not authorized <br>  Work Smart Limited");
			}
			file_put_contents($destination . "/index.html", "Not authorized <br>  Work Smart Limited");

		}

		$path = "{$destination}/{$filename}";

		if( file_exists($path) ){

			$senin = explode(".", $filename);
			$senin[0] .= "1";
			$filename = implode(".", $senin);
			$path = "{$destination}/{$filename}";
		}

		return "{$middlepath}{$foldername}/{$filename}";
	}


	public function getAppenedPath($Source, $Resolution_Size){
		$SourceArray = explode(".", $Source);
		array_splice($SourceArray, -1, 0, $Resolution_Size);
		return implode(".", $SourceArray);
	}


	/*  -----------------------------------------------------
	** end Download Image Helper functions
	----------------------------------------------------- */

} 




